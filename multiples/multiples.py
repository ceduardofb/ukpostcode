# program that prints the numbers from 1 to 100.
# But for multiples of three print “Three” instead of the number and for the multiples of five print “Five”.
# For numbers which are multiples of both three and five print “ThreeFive”
# To run : python multiples/multiples.py

import unittest


def multiple_of(number):
    # function to check if parameter is a multiple of 3 and/or 5. if not, return parameter

    if number % 3 == 0:
        out = 'Three'
    else:
        out = ''

    if number % 5 == 0:
        out += 'Five'

    return str(number) if out == '' else out


def multiples():
    # function to prints the numbers from 1 to 100, call function multiple_of to check if the actual number
    # is multiple of 3 and/or 5

    for x in range(1, 101):
        print(multiple_of(x))


class Test(unittest.TestCase):
    # Class to realize unit test with unittest framework
    def test(self):
        self.assertEqual(multiple_of(3), 'Three')  # multiple of 3
        self.assertEqual(multiple_of(2), '2')  # no multiple of 3 and 5
        self.assertEqual(multiple_of(10), 'Five')  # multiple of 5
        self.assertEqual(multiple_of(60), 'ThreeFive')  # multiple of 3 and 5


if __name__ == '__main__':
    multiples()
    # unittest.main()  # to realize unit test with unittest framework