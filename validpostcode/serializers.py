from rest_framework import serializers


class PostcodeSerializer(serializers.Serializer):
   postcode = serializers.CharField(max_length=8, help_text=("Uk post code"))