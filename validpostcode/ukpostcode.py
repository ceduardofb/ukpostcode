import re


class CkeckUkPostCode():

    def verify_format(self):
        regulare = '^([A-Za-z][A-Ha-hJ-Yj-y]?[0-9][A-Za-z0-9]? [0-9][A-Za-z]{2}|[Gg][Ii][Rr] 0[Aa]{2})$'

        re_compile = re.compile(regulare)

        if re_compile.match(self):
            return True
        else:
            return False

    def validation_code(self):

        if not CkeckUkPostCode.verify_format(self):
            return {'result': False, 'message':'Invalid format for code'}

        ONLY_SINGLE_DIGIT = ["B", "E", "G", "L", "M", "N", "S", "W", "BR", "FY", "HA", "HD", "HG", "HR", "HS", "HX",
                             "JE", "LD", "SM", "SR", "WC", "WN", "ZE", "CR"]

        ONLY_DOUBLE_DIGIT = ["B", "E", "G", "L", "M", "N", "S", "W", "AB", "LL", "SO", "BS", "DN", "CR"]
        ZERO_DIGIT = ["BL", "BS", "CM", "CR", "FY", "HA", "PR", "SL", "SS"]
        TEN_DIGIT = ["BS"]
        FIRST_LETTERS = ["Q", "V", "X"]
        SECOND_LETTERS = ["I", "J", "Z"]
        THIRD_LETTERS = ["A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "P", "S", "T", "U", "W"]
        FOURTH_LETTERS = ["A", "B", "E", "H", "M", "N", "P", "R", "V", "W", "X", "Y"]
        LAST_LETTERS = ["C", "I", "K", "M", "O", "V"]

        A9A = '^([A-Za-z]{1}?[0-9]{1}?[A-Za-z]{1})$'
        AA9A = '^([A-Za-z]{2}?[0-9]{1}?[A-Za-z]{1})$'

        first_part = self.split(' ')[0]
        second_part = self.split(' ')[1]

        only_numbers = re.findall(r'\d+', first_part)[0]
        only_chars = re.findall(r'\D+', first_part)[0]

        ultimate_letter = second_part[2]
        penultimate_letter = second_part[1]

        if len(only_numbers) == 1 and only_chars.upper() not in ONLY_SINGLE_DIGIT and only_numbers != "0":
            return {'result': False, 'message':'Invalid quantity digits 1 for code'}

        if len(only_numbers) == 2 and only_chars.upper() not in ONLY_DOUBLE_DIGIT:
            return {'result': False, 'message':'Invalid quantity digits 2 for code'}

        if only_numbers == "0" and only_chars.upper() not in ZERO_DIGIT:
            return {'result': False, 'message':'Invalid code for district 0 '}

        if only_numbers == "10" and only_chars.upper() not in TEN_DIGIT:
            return {'result': False, 'message':'Invalid code for district 10 '}

        if first_part[0].upper() in FIRST_LETTERS:
            return {'result': False, 'message':'Invalid first letters'}

        if first_part[1].upper() in SECOND_LETTERS:
            return {'result': False, 'message':'Invalid second letters'}

        re_compile = re.compile(A9A)

        if re_compile.match(first_part) and first_part[2].upper() not in THIRD_LETTERS:
            return {'result': False, 'message':'Invalid third letters'}

        re_compile = re.compile(AA9A)

        if re_compile.match(first_part) and first_part[3].upper() not in FOURTH_LETTERS:
            return {'result': False, 'message':'Invalid fourth letters'}

        if ultimate_letter.upper() in LAST_LETTERS or penultimate_letter.upper() in LAST_LETTERS:
            return {'result': False, 'message':'Invalid last letters'}

        return {'result': True, 'message':'Valid Post Code'}
