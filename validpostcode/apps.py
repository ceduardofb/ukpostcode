from django.apps import AppConfig


class ValidpostcodeConfig(AppConfig):
    name = 'validpostcode'
