from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from django.urls import reverse


class ViewTestCase(TestCase):
    """Test suite for the api views."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.client = APIClient()

        # self.user_data = {'postcode': 'CR2 6XH'}

        # self.user_data = {'postcode': 'CX2 6XH'}

        # self.user_data = {'postcode': 'CX2UUU 6XH'}

        self.user_data = {'postcode': 'ZZ2 6XH'}

        self.response = self.client.post(
            reverse('postcode'),
            self.user_data,
            format="json")


    def test_api_valid(self):
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)