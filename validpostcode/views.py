from rest_framework.response import Response
from rest_framework import status, generics
from validpostcode.ukpostcode import CkeckUkPostCode
from validpostcode.serializers import PostcodeSerializer


class PostCodeDetail(generics.CreateAPIView):

    serializer_class = PostcodeSerializer

    def post(self, request):
        """validate a uk format post code."""

        post_code = request.data.get('postcode')

        result = CkeckUkPostCode.validation_code(post_code)['result']
        message = CkeckUkPostCode.validation_code(post_code)['message']

        if result:
            return Response({"message": message}, status=status.HTTP_200_OK)
        else:
            return Response({"message": message}, status=status.HTTP_400_BAD_REQUEST)
